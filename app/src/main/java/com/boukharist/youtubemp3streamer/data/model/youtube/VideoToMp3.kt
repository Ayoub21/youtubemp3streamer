package com.boukharist.youtubemp3streamer.data.model.youtube


data class VideoToMp3Response(val title: String, //Happy Forever Alone Day (Forever Alone Song)
                              val length: String, //125
                              val link: String //http://convertmp3.io/download/get/?i=k1MakL%2FYlh6KUtEHqyQlg9XwtUUWO
)