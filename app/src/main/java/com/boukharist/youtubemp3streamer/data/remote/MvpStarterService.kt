package data.remote


import com.boukharist.youtubemp3streamer.data.model.youtube.VideoToMp3Response
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface MvpStarterService {

    @GET("fetch")
    fun getVideoByUrl(@Query("format") format: String, @Query("video") videoUrl: String): Single<VideoToMp3Response>

    companion object {
        const val YOUTUBE_VIDEO_URL_PREFIX: String = "https://www.youtube.com/watch?v="
    }

}
