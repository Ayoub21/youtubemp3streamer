package com.boukharist.youtubemp3streamer.data.model.youtube

import android.content.res.Configuration
import android.content.res.Resources
import com.google.api.services.youtube.model.ThumbnailDetails

data class Thumbnail(val small: String, val medium: String, val large: String) {


    constructor(thumbnailDetails: ThumbnailDetails) : this(thumbnailDetails.default.url, thumbnailDetails.medium.url, thumbnailDetails.high.url)

    fun getRightPictureUrl(): String {
        var screenLayout = Resources.getSystem().configuration.screenLayout
        screenLayout = screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK

        when (screenLayout) {
            Configuration.SCREENLAYOUT_SIZE_SMALL -> return this.small
            Configuration.SCREENLAYOUT_SIZE_NORMAL -> return this.medium
            Configuration.SCREENLAYOUT_SIZE_LARGE -> return this.large
            else -> return this.large
        }
    }
}