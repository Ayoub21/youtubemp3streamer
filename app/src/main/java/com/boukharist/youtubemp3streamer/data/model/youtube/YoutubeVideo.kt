package com.boukharist.youtubemp3streamer.data.model.youtube

import com.google.api.services.youtube.model.SearchResult
import com.tonyodev.fetch.Fetch

class YoutubeVideo(
        var youtubeVideoId: String,
        var title: String,
        var length: String,
        var link: String,
        var thumbnail: Thumbnail,
        var mDownloadingStatus: Int = Fetch.STATUS_DOWNLOADING,
        var downloadPercent: Int = 0,
        var lastEmittedDownloadProgress: Long = 0,
        var mDownloadId: Long = 0) {

    constructor(searchResult: SearchResult, videoToMp3Response: VideoToMp3Response) :
            this(searchResult.id.videoId, videoToMp3Response.title, videoToMp3Response.length, videoToMp3Response.link, Thumbnail(searchResult.snippet.thumbnails))


}


