package data

import android.app.Application
import com.boukharist.youtubemp3streamer.BuildConfig
import com.boukharist.youtubemp3streamer.R
import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.client.http.HttpRequestInitializer
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.SearchResult
import data.remote.MvpStarterService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject
constructor(private val mMvpStarterService: MvpStarterService, private val mContext: Application) {

    fun getYoutubeResultsByQuery(query: String): Single<List<YoutubeVideo>> {
        return Observable.just(1)
                .subscribeOn(Schedulers.io())
                .flatMap { getYoutubeVideosByQuery(query).toObservable() }
                .flatMapIterable { results -> results }
                .flatMapSingle { searchResult -> getSingleVideoInfo(searchResult) }
                .toList()
    }


    /*
    ************************************************************************************************
    ** Private Method
    ************************************************************************************************
    */

    private fun getYoutubeVideosByQuery(query: String): Single<List<SearchResult>> {

        try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.

            val youtube = YouTube.Builder(NetHttpTransport(), JacksonFactory(), HttpRequestInitializer { })
                    .setApplicationName(mContext.getString(R.string.app_name))
                    .build()

            // Define the API request for retrieving search results.
            val search = youtube.search().list("id,snippet")

            // Set your developer key from the Google Developers Console for
            // non-authenticated requests. See:
            // https://console.developers.google.com/
            search.key = BuildConfig.YOUTUBE_API_KEY
            search.q = query

            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.type = "video"

            // To increase efficiency, only retrieve the fields that the
            // application uses.
            //   search.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url,snippet/thumbnails/medium/url)");
            search.maxResults = 5L//AppConstants.NUMBER_OF_VIDEOS_RETURNED);

            // Call the API and print results.
            val searchResponse = search.execute()
            val searchResultList = searchResponse.items
            if (searchResultList != null) {
                return searchResultList.toList().toSingle()
            }
        } catch (e: GoogleJsonResponseException) {
            System.err.println("There was a service error: " + e.details.code + " : "
                    + e.details.message)
        } catch (e: IOException) {
            System.err.println("There was an IO error: " + e.cause + " : " + e.message)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
        return Single.just(emptyList());
    }


    private fun getSingleVideoInfo(searchResult: SearchResult): Single<YoutubeVideo> {
        return mMvpStarterService.getVideoByUrl("JSON", MvpStarterService.YOUTUBE_VIDEO_URL_PREFIX + searchResult.id.videoId)
                .map { videoToMp3Response -> YoutubeVideo(searchResult, videoToMp3Response) }
    }

}