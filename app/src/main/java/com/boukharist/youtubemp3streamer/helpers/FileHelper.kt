package com.boukharist.youtubemp3streamer.helpers

import android.os.Environment
import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import java.io.File


class FileHelper {
    companion object {
        val FILE_NAME_EXTENSION: String = ".mp3"
        val APP_FOLDER: String = "/youtubemp3"

        fun getDir(): String {
            val dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + APP_FOLDER
            val file = File(dirPath)
            file.mkdirs()
            return file.path;
        }

        fun getFilePath(youtubeVideo: YoutubeVideo): String {
            return getDir() + "/" + youtubeVideo.title + FILE_NAME_EXTENSION;
        }
    }
}