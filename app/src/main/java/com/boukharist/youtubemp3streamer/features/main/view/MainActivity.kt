package features.main

import android.Manifest
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import com.boukharist.youtubemp3streamer.R
import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import com.boukharist.youtubemp3streamer.features.common.ErrorView
import com.boukharist.youtubemp3streamer.features.main.adapter.VideosAdapter
import com.boukharist.youtubemp3streamer.features.main.callback.ExternalPermissionListener
import com.boukharist.youtubemp3streamer.features.main.callback.ItemDownloadCallback
import com.boukharist.youtubemp3streamer.helpers.FileHelper
import com.tedpark.tedpermission.rx2.TedRx2Permission
import com.tonyodev.fetch.Fetch
import com.tonyodev.fetch.listener.FetchListener
import com.tonyodev.fetch.request.Request
import features.base.BaseActivity
import timber.log.Timber
import javax.inject.Inject


class MainActivity : BaseActivity(), MainMvpView, ErrorView.ErrorListener, ItemDownloadCallback {


    /*
    ************************************************************************************************
    ** Private Fields
    ************************************************************************************************
    */

    lateinit var mVideosAdapter: VideosAdapter
    lateinit var mFetch: Fetch
    @Inject lateinit var mMainPresenter: MainPresenter


    /*
    ************************************************************************************************
    ** View Binding
    ************************************************************************************************
    */

    @BindView(R.id.view_error)
    @JvmField
    var mErrorView: ErrorView? = null
    @BindView(R.id.progress)
    @JvmField
    var mProgress: ProgressBar? = null
    @BindView(R.id.recycler_view)
    @JvmField
    var mRecycler: RecyclerView? = null
    @BindView(R.id.swipe_to_refresh)
    @JvmField
    var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    @BindView(R.id.toolbar)
    @JvmField
    var mToolbar: Toolbar? = null


    /*
    ************************************************************************************************
    ** Life cycle
    ************************************************************************************************
    */
    override fun init() {
        setupPresenter()
        initFetch();
        setupViews();
        setupData()
    }

    override val layout: Int
        get() = R.layout.activity_main

    override fun onDestroy() {
        super.onDestroy()
        mMainPresenter.detachView()
    }

    override fun onStop() {
        super.onStop()
        if (isFinishing) {
            mFetch.release()
        }
    }


    /*
    ************************************************************************************************
    ** ItemDownloadCallback Implementation
    ************************************************************************************************
    */

    override fun startDownloading(url: String, fileName: String): Long {

        val dir = FileHelper.getDir()
        val request = Request(url, dir, fileName)
        if (mFetch.contains(request)) {
            val id = mFetch.get(request)?.id!!
            mFetch.removeRequest(id);
        }
        return mFetch.enqueue(request)
    }

    override fun addFetchListener(fetchListener: FetchListener) {
        if (mMainPresenter.isViewAttached) {
            Timber.d("Add fetch listener")
            mFetch.addFetchListener(fetchListener)
        }
    }

    override fun askForPermission(permissionListener: ExternalPermissionListener) {
        if (mMainPresenter.isViewAttached) {
            TedRx2Permission.with(this)
                    .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .request()
                    .subscribe({ tedPermissionResult ->
                        if (tedPermissionResult.isGranted()) {
                            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                            permissionListener.onPermissionGranted()
                        } else {
                            Toast.makeText(this,
                                    "Permission Denied\n" + tedPermissionResult.getDeniedPermissions().toString(), Toast.LENGTH_SHORT)
                                    .show()
                            permissionListener.onPermissionDenied()
                        }
                    }, { _ -> }, { })
        }
    }

    override fun hasPermission(): Boolean {
        return TedRx2Permission.isGranted(this, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    }


    /*
    ************************************************************************************************
    ** MainMvpView Implementation
    ************************************************************************************************
    */

    override fun showProgress(show: Boolean) {
        if (show) {
            if (mRecycler?.visibility == View.VISIBLE) {
                mSwipeRefreshLayout?.isRefreshing = true
            } else {
                mProgress?.visibility = View.VISIBLE

                mRecycler?.visibility = View.GONE
                mSwipeRefreshLayout?.visibility = View.GONE
            }

            mErrorView?.visibility = View.GONE
        } else {
            mSwipeRefreshLayout?.isRefreshing = false
            mProgress?.visibility = View.GONE
        }
    }

    override fun showError(error: Throwable) {
        mRecycler?.visibility = View.GONE
        mSwipeRefreshLayout?.visibility = View.GONE
        mErrorView?.visibility = View.VISIBLE
        Timber.e(error, "There was an error retrieving the pokemon")
    }

    override fun showVideosInfo(videos: List<YoutubeVideo>) {
        mVideosAdapter = VideosAdapter(videos, this)
        mRecycler?.adapter = mVideosAdapter
    }

    override fun onReloadData() {
        mMainPresenter.getResults()
    }


    /*
    ************************************************************************************************
    ** Private Functions
    ************************************************************************************************
    */

    private fun setupData() {
        mMainPresenter.getResults()
    }

    private fun setupViews() {
        setSupportActionBar(mToolbar)

        mSwipeRefreshLayout?.setProgressBackgroundColorSchemeResource(R.color.primary)
        mSwipeRefreshLayout?.setColorSchemeResources(R.color.white)
        mSwipeRefreshLayout?.setOnRefreshListener { mMainPresenter.getResults() }

        mRecycler?.layoutManager = LinearLayoutManager(this)

        mErrorView?.setErrorListener(this)
    }

    private fun setupPresenter() {
        activityComponent().inject(this)
        mMainPresenter.attachView(this)
    }

    private fun initFetch() {
        //init fetch
        mFetch = Fetch.newInstance(this)

        //set settings
        Fetch.Settings(this)
                .setAllowedNetwork(Fetch.NETWORK_ALL)
                .enableLogging(true)
                .setConcurrentDownloadsLimit(1)
                .apply()

        //clear fetch
        mFetch.removeRequests();
    }

}