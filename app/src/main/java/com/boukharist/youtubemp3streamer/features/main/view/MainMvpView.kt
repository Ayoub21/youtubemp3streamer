package features.main

import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import features.base.MvpView

interface MainMvpView : MvpView {

    fun showProgress(show: Boolean)

    fun showError(error: Throwable)

    fun showVideosInfo(videos: List<YoutubeVideo>)

}