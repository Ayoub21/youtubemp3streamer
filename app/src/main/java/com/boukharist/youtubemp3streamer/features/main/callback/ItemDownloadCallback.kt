package com.boukharist.youtubemp3streamer.features.main.callback

import com.tonyodev.fetch.listener.FetchListener


interface ItemDownloadCallback {
    fun startDownloading(url: String, fileName: String): Long
    fun addFetchListener(fetchListener: FetchListener)
    fun askForPermission(permissionListener: ExternalPermissionListener)
    fun hasPermission(): Boolean
}