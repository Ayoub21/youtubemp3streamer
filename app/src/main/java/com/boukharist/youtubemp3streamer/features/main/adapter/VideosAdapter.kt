package com.boukharist.youtubemp3streamer.features.main.adapter

import android.view.View
import android.view.ViewGroup
import com.boukharist.youtubemp3streamer.R
import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import com.boukharist.youtubemp3streamer.features.main.callback.ItemDownloadCallback
import com.boukharist.youtubemp3streamer.features.main.holder.YoutubeVideoViewHolder
import com.chad.library.adapter.base.BaseQuickAdapter
import io.reactivex.annotations.NonNull


class VideosAdapter : BaseQuickAdapter<YoutubeVideo, YoutubeVideoViewHolder> {


    /*
    ************************************************************************************************
    ** Private Fields
    ************************************************************************************************
    */


    private val mItemDownloadCallback: ItemDownloadCallback


    /*
    ************************************************************************************************
    ** Constructor
    ************************************************************************************************
    */

    constructor(@NonNull youtubeSearchResults: List<YoutubeVideo>,
                @NonNull itemDownloadCallback: ItemDownloadCallback) : super(R.layout.item_video, youtubeSearchResults) {
        mItemDownloadCallback = itemDownloadCallback
    }


    /*
    ************************************************************************************************
    ** Public Methods
    ************************************************************************************************
    */

    public fun stopAllMedia(){
     //   this.viewho
    }

    /*
    ************************************************************************************************
    ** BaseQuickAdapter Implementation
    ************************************************************************************************
    */
    override fun createBaseViewHolder(parent: ViewGroup?, layoutResId: Int): YoutubeVideoViewHolder {
        val view: View = mLayoutInflater.inflate(layoutResId, parent, false)
        return YoutubeVideoViewHolder(view, mItemDownloadCallback)
    }

    override fun convert(viewHolder: YoutubeVideoViewHolder, item: YoutubeVideo) {
        viewHolder.bind(item)
    }


}