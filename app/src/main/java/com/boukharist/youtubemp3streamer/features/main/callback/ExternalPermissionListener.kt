package com.boukharist.youtubemp3streamer.features.main.callback


interface ExternalPermissionListener {
    fun onPermissionGranted()
    fun onPermissionDenied()
}