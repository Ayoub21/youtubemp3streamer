package com.boukharist.youtubemp3streamer.features.main.holder

import android.support.v7.widget.AppCompatImageView
import android.view.View
import com.boukharist.youtubemp3streamer.R
import com.boukharist.youtubemp3streamer.data.model.youtube.YoutubeVideo
import com.boukharist.youtubemp3streamer.features.main.callback.ExternalPermissionListener
import com.boukharist.youtubemp3streamer.features.main.callback.ItemDownloadCallback
import com.boukharist.youtubemp3streamer.features.main.widget.AudioPlayerView
import com.boukharist.youtubemp3streamer.helpers.FileHelper
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseViewHolder
import com.fenjuly.library.ArrowDownloadButton
import com.tonyodev.fetch.Fetch
import com.tonyodev.fetch.listener.FetchListener
import timber.log.Timber


class YoutubeVideoViewHolder(view: View, private val mCallback: ItemDownloadCallback) : BaseViewHolder(view), FetchListener {


    /*
    ************************************************************************************************
    ** Private Fields
    ************************************************************************************************
    */


    lateinit var mYoutubeVideo: YoutubeVideo

    val mDownloadButton: ArrowDownloadButton
    val mAudioPlayerButton: AudioPlayerView


    /*
    ************************************************************************************************
    ** LifeCycle Methods
    ************************************************************************************************
    */

    init {
        mDownloadButton = getView(R.id.arrow_download_button)
        mDownloadButton.setOnClickListener({
            onDownloadClicked()
        })
        mAudioPlayerButton = getView(R.id.audio_player_view)

        mAudioPlayerButton.setOnClickListener({
            onPlayClicked()
        })
    }

    fun stopMedia() {
        mAudioPlayerButton.stop();
    }

    private fun onPlayClicked() {
        if (!mAudioPlayerButton.isPlaying) {
          //  mCallback.stopAllCurrentlyPlayingMedia()
        }

        mAudioPlayerButton.withUrl(mYoutubeVideo.link)
        mAudioPlayerButton.toggleAudio()
    }

    private fun onDownloadClicked() {
        if (mDownloadButton.progress == 0F) {
            //Only when the icon is in not downloaded state, then do the following.
            if (mCallback.hasPermission()) {
                startDownloading()
            } else {
                mCallback.askForPermission(object : ExternalPermissionListener {
                    override fun onPermissionGranted() {
                        startDownloading()
                    }

                    override fun onPermissionDenied() {
                        Timber.d("Permission Denied")
                    }
                })
            }
        }
    }


    /*
    ************************************************************************************************
    ** Public Methods
    ************************************************************************************************
    */


    fun bind(item: YoutubeVideo) {
        mYoutubeVideo = item

        //set title
        this.setText(R.id.title_text_view, item.title)

        //set length
        this.setText(R.id.duration_text_view, item.length)


        //set thumbnail
        Glide.with(itemView.context)
                .load(item.thumbnail.getRightPictureUrl())
                .crossFade()
                .into(this.getView<AppCompatImageView>(R.id.thumbnail_image_view))

        //set download button
    }

    /*
    ************************************************************************************************
    ** Private Functions
    ************************************************************************************************
    */

    private fun onDownloadCompleted() {
        Timber.d("onDownloadCompleted")

        //progress finished
        mDownloadButton.progress = 100.0f
        val filePath = FileHelper.getFilePath(mYoutubeVideo)
        Timber.d("onDownloadCompleted >> " + filePath)
    }

    private fun onDownloadProgress(progress: Int) {
        Timber.d("setProgressState")
        mDownloadButton.startAnimating()
        mDownloadButton.setProgress(progress.toFloat())
    }

    private fun startDownloading() {
        Timber.d("startDownloading")

        val url = mYoutubeVideo.link
        val fileName = mYoutubeVideo.title + FileHelper.FILE_NAME_EXTENSION


        mYoutubeVideo.mDownloadId = mCallback.startDownloading(url, fileName)

        if (mYoutubeVideo.mDownloadId != Fetch.ENQUEUE_ERROR_ID.toLong()) {
            mCallback.addFetchListener(this);
            //Download was successfully queued for download.
            mDownloadButton.startAnimating()
            mDownloadButton.progress = 0F
            Timber.d("startDownloading >> success")
        } else {
            Timber.d("startDownloading >> error")
        }
    }


    private fun onDownloadError() {
        mDownloadButton.reset()
    }

    /*
    ************************************************************************************************
    ** FetchListener Implementation
    ************************************************************************************************
    */

    /**
     * This method is called by an instance of Fetch to notify the listener
     * of status and progress changes of requests managed by the FetchService.
     *
     * @param id a unique ID used by Fetch and the FetchService to identify a download
     * request.
     *
     * @param status download status of a request.
     * @param progress progress/percentage of a request.
     * @param downloadedBytes downloaded file bytes.
     * @param fileSize total file size.
     * @param error error code if the download status is STATUS_ERROR.
     * Default value is -1(NO ERROR).
     */
    override fun onUpdate(id: Long, status: Int, progress: Int, downloadedBytes: Long, fileSize: Long, error: Int) {

        if (id == mYoutubeVideo.mDownloadId) {
            if (status == Fetch.STATUS_DONE) {
                Timber.d("onUpdate >> downloaded" + id)
                onDownloadCompleted()
            } else if (id == mYoutubeVideo.mDownloadId && status == Fetch.STATUS_DOWNLOADING) {
                Timber.d("onUpdate >> downloading" + id)
                onDownloadProgress(progress)
            } else if (error != Fetch.NO_ERROR) {
                //An error occurred
                Timber.d("onUpdate >> error " + error)
                onDownloadError()
            }
        }

    }

}
