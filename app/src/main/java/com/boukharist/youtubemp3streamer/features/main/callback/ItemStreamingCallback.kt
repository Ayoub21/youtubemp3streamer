package com.boukharist.youtubemp3streamer.features.main.callback

interface ItemStreamingCallback {
    fun stopAllPlayingMedia()
}