package features.main


import data.DataManager
import features.base.BasePresenter
import injection.ConfigPersistent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@ConfigPersistent
class MainPresenter @Inject
constructor(private val mDataManager: DataManager) : BasePresenter<MainMvpView>() {

    override fun attachView(mvpView: MainMvpView) {
        super.attachView(mvpView)
    }

    fun getResults() {
        checkViewAttached()
        mvpView?.showProgress(true)

        mDataManager.getYoutubeResultsByQuery("jay z the story of oj")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate({ mvpView?.showProgress(false) })
                .subscribe({ videos ->
                    mvpView?.showVideosInfo(videos)
                }) { throwable ->
                    mvpView?.showError(throwable)
                }

    }
}