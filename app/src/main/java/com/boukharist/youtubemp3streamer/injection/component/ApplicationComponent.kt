package injection.component

import android.app.Application
import android.content.Context
import dagger.Component
import data.DataManager
import data.remote.MvpStarterService
import injection.ApplicationContext
import injection.module.ApplicationModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    @ApplicationContext
    fun context(): Context

    fun application(): Application

    fun dataManager(): DataManager

    fun mvpBoilerplateService(): MvpStarterService
}
