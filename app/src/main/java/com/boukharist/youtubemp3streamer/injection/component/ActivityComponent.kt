package injection.component

import dagger.Subcomponent
import features.base.BaseActivity
import features.main.MainActivity
import injection.PerActivity
import injection.module.ActivityModule

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(baseActivity: BaseActivity)

    fun inject(mainActivity: MainActivity)
}
